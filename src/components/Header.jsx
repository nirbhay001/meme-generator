import React from 'react'

function Header() {
  return (
    <>
      <div className='header'>
        <img alt='troll face' className='image' src='./images/TrollFace.png'/>
        <div className='headerText'>Meme Generator</div>
        <h3 className='headerTitle'>React Cource Project-3</h3>
      </div>
    </>
  )
}

export default Header
