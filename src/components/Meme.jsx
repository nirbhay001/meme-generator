import React from "react";

function Meme() {
    const [meme, setMeme] = React.useState({
        topText: "",
        bottomText: "",
        randomImage: "http://i.imgflip.com/1bij.jpg" 
    })

    const [allMemes, setAllMemes] = React.useState([])

  function handleChange(event){
    const {name, value} = event.target
    setMeme(prevmeme=>({
            ...prevmeme,
            [name]:value
    }))
  }

    
  React.useEffect( () => {
    async function fetchData(){
        const res = await fetch("https://api.imgflip.com/get_memes")
        const data = await res.json()
        setAllMemes(data.data.memes)
    }
    fetchData()
  }, [])

  function getMemeImage() {
    console.log(allMemes);
    const randomNumber = Math.floor(Math.random()* allMemes.length)
    const url = allMemes[randomNumber].url
    setMeme(prevMeme => ({
        ...prevMeme,
        randomImage: url
    }))
    
}
  return (
    <>
      <div className="meme">
        <input
          type="text"
          placeholder="Top text"
          name="topText"
          value={meme.topText}
          onChange={handleChange}
        />
        <input
          type="text"
          placeholder="Bottom text"
          name="bottomText"
          value={meme.bottomText}
          onChange={handleChange}
        />
        
        <button className="button" onClick={getMemeImage} type="submit">
          Get a new meme image
        </button>
      </div>
      <div className="memepicture">
        <img alt="memeimage" className="memeimg" src={meme.randomImage} />
        <h1 className="memeText top">{meme.topText}</h1>
        <h1 className="memeText bottom">{meme.bottomText}</h1>
      </div>
    </>
  );
}

export default Meme;
